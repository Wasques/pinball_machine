﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class multipliers : MonoBehaviour {

    private float speed = 5f;
    private float contador = 0;
    private bool down = false;
    private Collider c;

	// Use this for initialization
	void Start () {
        c = GetComponent<Collider>();
	}
	
	// Update is called once per frame
	void Update () {
        if (down == true)
        {
            transform.Translate(0, -speed*Time.deltaTime, 0);
            contador += 5;
        }
        if (contador >= 50)
        {
            down = false;
            contador = 0;
        }
	}

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "sphere")
        {
            c.enabled = false;
            down = true; 
        }
    }
}
