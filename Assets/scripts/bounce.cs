﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bounce : MonoBehaviour {

    public float forceX = 500;
    //public float forceFlipOK = 500;
    private Rigidbody rb;
    private Vector3 direction;
    public bool effecting = false;
    public gameOver script;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>(); 
    }
	
	// Update is called once per frame
	void Update () {
        if (script.inst)
        {
            transform.position = new Vector3(5.19f, 0.688f, -7.18f);
            script.inst = false;
        }
	}

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "bumper")
        {
            Vector3 direction = col.contacts[0].point - transform.position;
            direction = -direction.normalized;
            //Debug.Log(direction * forceX);
            rb.AddForce(direction*forceX);
        }
        if (col.gameObject.tag == "x3")
        {
            Vector3 direction = col.contacts[0].point - transform.position;
            direction = -direction.normalized;
            //Debug.Log(direction * forceX);
            rb.AddForce(direction * forceX);
        }
        if (col.gameObject.tag == "puller")
        {
            effecting = true;
        }
        
        
    }
}
