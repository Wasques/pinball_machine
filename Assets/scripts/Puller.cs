﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puller : MonoBehaviour {

    private int maxDist = 35;
    public float actualDist = 1;
    public float plus = 0;
    public Rigidbody sphere;
    public GameObject puller;
    public bounce script;

	// Use this for initialization
	void Start () {
     
	}
    
	
	// Update is called once per frame
	void Update () {
        if (plus < maxDist)
        {
            if (Input.GetKey(KeyCode.Space))
            {
                plus += actualDist;
                puller.transform.Translate(0, -actualDist * Time.deltaTime, 0);
            }
        } 
        if (Input.GetKeyUp(KeyCode.Space))
            {
                puller.transform.Translate(0, plus * Time.deltaTime, 0);
                if(script.effecting){
                    sphere.AddForce(transform.up * 300 * (plus / 10));
                    script.effecting = false;
                }
                plus = 0;
            }
	}
}
