﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fliperSystem : MonoBehaviour {

    private Rigidbody rb;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.RightArrow))
        {
            rb.AddForce(-transform.up * 8000);
        }
	}
}
